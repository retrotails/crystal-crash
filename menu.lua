function init.menu()
	setMusicVolume(0.075)
	playMusic(save.music)
	ram = {
		option = 'main',
		cursor = 1,
		main = {
			-- 1
			{
				name = 'play',
				func = function()
					setstate('game', false)
				end
			},
			-- 2
			{
				name = 'new game',
				func = function()
					playing = false
					gram = nil
					setstate('game', true)
				end
			},
			-- 3
			{
				name = 'settings',
				func = function()
					ram.cursor = 1
					ram.option = 'settings'
				end
			},
			-- 4
			{
				name = 'controller',
				func = function()
					if controller.count > 0 then
						ram.cursor = 1
						ram.option = 'controller'
					end
				end
			},
			-- 5
			{
				name = 'scores',
				func = function()
					ram.cursor = 1
					ram.option = 'scores'
				end
			},
			-- 6
			{
				name = 'exit',
				func = function()
					love.event.quit()
				end
			},
		},
		controller = {},
		areyousure = {
			{
				name = 'yes',
				func = function()
					playing = false
					ram.option = 'settings'
					local ays = ram.areyousure
					save[ays.option] = ays.value
					if ays.option == 'difficulty' then
						ram.cursor = 8
					else
						ram.cursor = 9
					end
					savedata()
					ram.regenerate()
				end
			},
			{
				name = 'no',
				func = function()
					ram.option = 'settings'
					if ram.areyousure.option == 'difficulty' then
						ram.cursor = 8
					else
						ram.cursor = 9
					end
					savedata()
					ram.regenerate()
				end
			},
			option = 8,
			value = 1,
			skipcheck = function()
				if playing then
					ram.option = 'areyousure'
					ram.cursor = 2
				else
					ram.areyousure[1].func()
				end
			end
		},
		settings = {
			--name: Text to display
			--savename: save[savename] will be set to options.value upon saving
			--options:
				--name: description to display
				--value: value to set save[savename]
				--selected: which one of the settings is currently selected
				--func: code to run upon being selected
			-- 1
			{
				name = 'music',
				savename = 'music',
				selected = 1,
				options = {
				},
			},
			-- 2
			{
				name = 'controls',
				savename = 'touch',
				selected = 1,
				options = {
					{
						name = 'default',
						value = 1,
						func = function()
							save.touch = 1
							control.resize()
						end
					},
					{
						name = 'tablet',
						value = 2,
						func = function()
							save.touch = 2
							control.resize()
						end
					},
					--[[
					{
						name = 'swipe',
						value = 3,
						func = function()
							save.touch = 3
							control.resize()
						end
					},
					{
						name = 'swipe 2',
						value = 4,
						func = function()
							save.touch = 4
							control.resize()
						end
					},
					--]]
					{
						name = 'no touch',
						value = 5,
						func = function()
							save.touch = 5
							control.resize()
						end
					},
				},
			},
			-- 3
			{
				name = 'touch size',
				savename = 'touchscale',
				selected = 1,
				options = {
					{
						name = '1x',
						value = 32,
						func = function()
							save.touchscale = 32
							control.resize()
						end
					},
					{
						name = '1.5x',
						value = 48,
						func = function()
							save.touchscale = 48
							control.resize()
						end
					},
					{
						name = '2x',
						value = 64,
						func = function()
							save.touchscale = 64
							control.resize()
						end
					},
				},
			},
			-- 4
			{
				name = 'sound fx',
				savename = 'sfx',
				selected = 1,
				options = {
					{
						name = 'enabled',
						value = true,
						func = function()
							save.sfx = true
						end
					},
					{
						name = 'disabled',
						value = false,
						func = function()
							save.sfx = false
						end
					},
				},
			},
			-- 5
			{
				name = 'background',
				savename = 'bg',
				selected = 1,
				options = {
				},
			},
			-- 6
			{
				name = 'gem skin',
				savename = 'gem',
				selected = 1,
				options = {
				},
			},
			-- 7
			{
				name = 'joystick',
				savename = 'stick',
				selected = 1,
				options = {
				},
			},
			-- 8
			{
				name = 'difficulty',
				savename = 'difficulty',
				selected = 1,
				options = {
					{
						name = 'easy',
						value = 1,
						func = function()
							ram.areyousure.option = 'difficulty'
							ram.areyousure.value = 1
							ram.areyousure.skipcheck()
						end
					},
					{
						name = 'normal',
						value = 2,
						func = function()
							ram.areyousure.option = 'difficulty'
							ram.areyousure.value = 2
							ram.areyousure.skipcheck()
						end
					},
					{
						name = 'hard',
						value = 3,
						func = function()
							ram.areyousure.option = 'difficulty'
							ram.areyousure.value = 3
							ram.areyousure.skipcheck()
						end
					},
				},
			},
			-- 9
			{
				name = 'mode',
				savename = 'mode',
				selected = 1,
				options = {
					{
						name = 'original',
						value = 'normal',
						func = function()
							ram.areyousure.option =	 'mode'
							ram.areyousure.value = 'normal'
							ram.areyousure.skipcheck()
						end
					},
					{
						name = 'flash',
						value = 'flash',
						func = function()
							ram.areyousure.option = 'mode'
							ram.areyousure.value = 'flash'
							ram.areyousure.skipcheck()
						end
					},
				},
			},
		},
		scores = {
			{
				name = 'exit',
				func = function()
					ram.cursor = 1
					ram.option = 'main'
				end
			},
		},
		wait = 0, -- "Press a key to assign" timer
		gemrotate = 0, -- Timer for main menu rotating gems
		ctime = 0, -- Cursor pulse
	}

	--Generate music config
	local j = 0
	for i, k in pairs(music) do
		if j == 0 then
			j = 1
			ram.settings[1].options[j] = {
				name = 'disable',
				value = 'disable',
				func = function()
					save.music = 'disable'
					playMusic('disable')
				end
			}
		end
		j = j + 1
		ram.settings[1].options[j] = {
			name = i,
			value = i,
			func = function()
				save.music = i
				playMusic(i)
				setMusicVolume(0.075)
			end
		}
	end

	--Generate BG config
	local j = 0
	local a = 0
	local fs = love.filesystem.getDirectoryItems('bg')
	for i, k in ipairs(fs) do
		-- If the BG is live...
		local ext = string.sub(k, -4)
		if ext == '.lua' or ext == '.shd' then
			a = a + 1
			ram.settings[5].options[a] = {
				name = string.sub(string.lower(string.sub(k, 1, -5)), 1, 10),
				value = k,
				func = function()
					save.bg = k
					save.livebg = true
					bgcode = assert(loadstring(love.filesystem.read('bg/' .. save.bg)))()
					bgcode.init()
					collectgarbage()
				end
			}
		else -- It must be just an image.
			a = a + 1
			ram.settings[5].options[a] = {
				name = string.sub(string.lower(string.sub(k, 1, -5)), 1, 10),
				value = k,
				func = function()
					save.bg = k
					save.livebg = false
					collectgarbage()
				end
			}
		end
	end

	--Generate gems config
	local j = 0
	for i, k in pairs(tex.gems) do
		j = j + 1
		ram.settings[6].options[j] = {
			name = '',--i,
			value = i,
			func = function()
				save.gem = i
			end
		}
	end

	if controller.count > 0 then
		--Generate selected joystick config
		local j = 0
		for i, k in pairs(controller.object) do
			j = j + 1
			ram.settings[7].options[j] = {
				name = string.sub(string.lower(controller.object[i]:getName()), 1, 8),
				value = i,
				func = function()
					save.stick = i
				end
			}
		end

		--Generate controller config
		for i, k in ipairs(save.controller) do
			ram.controller[i] = {
				name = save.controller[i].name,
				func = function()
					if controller.count > 0 then
						ram.wait = 5
					end
				end
			}
		end
	else
		ram.settings[7].options[1] = {
			name = 'none',
			value = 1,
			func = function()
			end
		}
	end

	-- Figure out which settings should be selected
	function ram.regenerate()
		for j, l in ipairs(ram.settings) do
			for i, k in pairs(ram.settings[j].options) do
				if k.value == save[l.savename] then
					ram.settings[j].selected = i
					break
				end
			end
		end
	end
	ram.regenerate()
end

function update.menu(dt)
	control.update(dt, true)
	if save.livebg then
		bgcode.update(dt)
	end
	if ram.wait > 0 then
		if ram.tmpkey then
			save.controller[ram.cursor].button = ram.tmpkey
			ram.tmpkey = nil
			ram.wait = 0
		else
			ram.wait = math.max(ram.wait - dt, 0)
		end
	end
	ram.gemrotate = (ram.gemrotate + dt * 2)%(math.pi*2)
	ram.ctime = (ram.ctime + dt)%1
end

function draw.menu()
	-- Draw BG
	if save.livebg then
		bgcode.draw()
	else
		local scale = math.ceil(math.max(screen.w, screen.h)/512)
		love.graphics.draw(tex.bg[save.bg], screen.w * 0.5 - scale * 256, screen.h * 0.5 - scale * 256, 0, scale)
	end
	-- Make everything below darker

	love.graphics.setColor(0, 0, 0, 0.5)
	love.graphics.rectangle('fill', 0, 0, screen.w, screen.h)
	love.graphics.setColor(1/4, 1/4, 1/4, 1)

	-- Draw touch controls
	if ram.option == 'settings' then
		control.draw()
	end

	-- Start 320x240 graphics
	love.graphics.setColor(1,1,1,1)
	love.graphics.setCanvas(canvas[1])

	-- Draw 2nd BG
	if ram.option == 'settings' then
		--love.graphics.draw(tex.menu.settings)
	elseif ram.option == 'main' then
		love.graphics.draw(tex.menu.title)
		local j = ram.gemrotate
		for i=1,6 do
			j = j + math.pi/3
			love.graphics.draw(tex.gems[save.gem], quad.gem[i], math.sin(j)*78 + 160, math.sin(j+math.pi/2)*78 + 152, -j, 1, 1, 8, 8)
		end
	end

	-- Draw cursor
	local pos = {}
	if ram.option == "main" then
		pos = {
			96,
			ram.cursor*16 + 90,
			128,
			16
		}
	elseif ram.option == "scores" then
		pos = {
			0,
			220,
			79,
			14
		}
	elseif ram.option == "settings" then
		if ram.cursor < 6 then
			pos = {
				32,
				((ram.cursor - 1) * 50) + 4,
				96,
				32
			}
		else
			pos = {
				192,
				((ram.cursor - 6) * 50) + 4,
				96,
				32
			}
		end
	elseif ram.option == "controller" then
		pos = {
			94,
			ram.cursor*16 + 62,
			64,
			14
		}
	elseif ram.option == "areyousure" then
		pos = {
			128,
			ram.cursor*16 + 128,
			64,
			16
		}
	end
	local s = math.cos(ram.ctime*6.283)
	love.graphics.setColor(0.5, 3/4, 1, s/8 + 3/4)
	love.graphics.rectangle(
		'fill',
		pos[1] + s,
		pos[2] + s,
		pos[3] - s*2,
		pos[4] - s*2
	)
	love.graphics.setColor(1,1,1)

	if ram.option == 'settings' then
		-- Red warking square for settings that will restart the game
		if playing then
			love.graphics.setColor(1,3/8,3/8,1/4)
			love.graphics.rectangle('fill', 160, 96, 160, 96)
			love.graphics.setColor(1,1,1,1)
		end
		for i, k in ipairs(ram.settings) do
			if i <= 5 then
				drawText(k.name, 80, (i - 1) * 50 + 8, 'center')
				love.graphics.setColor(3/4, 0.5, 1)
				drawText(k.options[k.selected].name, 80, (i - 1) * 50 + 22, 'center')
				love.graphics.setColor(1,1,1)
				love.graphics.draw(tex.arrow, 0, (i - 1) * 50 + 8, 0, 2)
				love.graphics.draw(tex.arrow, 160, (i - 1) * 50 + 8, 0, -2, 2)
			else
				local i = i - 5
				drawText(k.name, 240, (i - 1) * 50 + 8, 'center')
				love.graphics.setColor(3/4, 0.5, 1)
				drawText(k.options[k.selected].name, 240, (i - 1) * 50 + 22, 'center')
				love.graphics.setColor(1,1,1)
				love.graphics.draw(tex.arrow, 160, (i - 1) * 50 + 8, 0, 2)
				love.graphics.draw(tex.arrow, 320, (i - 1) * 50 + 8, 0, -2, 2)
			end
		end
		-- Draw saved gem
		--love.graphics.draw(tex.gems[save.gem], board.previewquad, 192, 20)
		for i = 1,(3+ram.settings[8].selected) do
			love.graphics.draw(tex.gems[save.gem], quad.gem[i], 192 + (i-1) * 16, 20)
		end
	elseif ram.option == 'controller' then
		if ram.wait > 0 then
			drawText('waiting for input... ' .. math.ceil(ram.wait), 96, 56)
		end
		for i, k in ipairs(ram.controller) do
			-- Names
			drawText(
				k.name,
				96,
				64+i*16
			)
			-- Mappings
			drawText(
				save.controller[i].button,
				168,
				64+i*16
			)
			-- 'Clear' buttons
			love.graphics.draw(tex.x, 212, 56 + i*16)
		end
	elseif ram.option == 'scores' then
		drawText('exit', 4, 224)
		drawText('original', 16, 16)

		drawText('easy', 32, 32)
		drawText(save.scores.normal[1].name, 96, 32)
		drawText(save.scores.normal[1].score, 176, 32)

		drawText('normal', 32, 64)
		drawText(save.scores.normal[2].name, 96, 64)
		drawText(save.scores.normal[2].score, 176, 64)

		drawText('hard', 32, 96)
		drawText(save.scores.normal[3].name, 96, 96)
		drawText(save.scores.normal[3].score, 176, 96)

		drawText('flash', 16, 112)

		drawText('easy', 32, 128)
		drawText(save.scores.flash[1].name, 96, 128)
		drawText('l'..save.scores.flash[1].level, 176, 128)
		drawText(getTime(save.scores.flash[1].score), 218, 128)

		drawText('normal', 32, 160)
		drawText(save.scores.flash[2].name, 96, 160)
		drawText('l'..save.scores.flash[2].level, 176, 160)
		drawText(getTime(save.scores.flash[2].score), 218, 160)

		drawText('hard', 32, 192)
		drawText(save.scores.flash[3].name, 96, 192)
		drawText('l'..save.scores.flash[3].level, 176, 192)
		drawText(getTime(save.scores.flash[3].score), 218, 192)

	elseif ram.option == 'main' then
		for i, k in ipairs(ram.main) do
			drawText(
				k.name,
				160,
				94+i*16,
				'center'
		 )
		end
	elseif ram.option == 'areyousure' then
		drawText('this will reset your game,', 160, 64, 'center')
		drawText('are you sure?', 160, 96, 'center')
		drawText('yes', 160, 148, 'center')
		drawText('no', 160, 164, 'center')
	end
	--End 320x240 graphics
	love.graphics.setCanvas(canvas[2])
	love.graphics.draw(canvas[1], 0, 0, 0, 4)
	love.graphics.setCanvas()
	love.graphics.draw(canvas[2], screen.offset.x, screen.offset.y, 0, screen.scale/4)

	-- Clear canvases
	love.graphics.setCanvas(canvas[1])
	love.graphics.clear()
	love.graphics.setCanvas(canvas[2])
	love.graphics.clear()
	love.graphics.setCanvas()

	-- Back button
	-- love.graphics.draw(tex.x, screen.w - 32*screen.scale, 0, 0, screen.scale*2)
end

function keypress.menu(key)
	--If we aren't waiting for a button press for the controller configuration...
	if ram.wait == 0 then
		if key == 'up' then
			ram.cursor = math.max(ram.cursor - 1, 1)
		elseif key == 'down' then
			ram.cursor = math.min(ram.cursor + 1, #ram[ram.option])
		elseif key == ' ' or key == 'return' then
			if ram.option ~= 'settings' then
				ram[ram.option][ram.cursor].func()
			end
		elseif key == 'right' then
			if ram.option == 'settings' then
				-- Difficulty setting and mode setting, because they need to be explicitly set.
				local i = ram.cursor
				ram.settings[i].selected = (ram.settings[i].selected) % #ram.settings[i].options + 1
				ram.settings[i].options[ram.settings[i].selected].func()
			end
		elseif key == 'left' then
			if ram.option == 'settings' then
				-- Difficulty setting and mode setting, because they need to be explicitly set.
				local i = ram.cursor
				ram.settings[i].selected = (ram.settings[i].selected - 2) % #ram.settings[i].options + 1
				ram.settings[i].options[ram.settings[i].selected].func()
			end
		elseif key == 'escape' then
			if ram.option ~= 'main' then
				ram.option = 'main'
				ram.cursor = 1
				savedata()
			else
				if playing then
					setstate('game', false)
					return
				else
					love.event.quit()
				end
			end
		end
	end
end

function buttonpress.menu(button)
	if ram.wait > 0 then
		ram.tmpkey = button
	end
end

function mousepress.menu(x, y, button)
	-- Back button
	-- if x > screen.w - 32 * screen.scale and x < screen.w and y > 0 and y < 32 * screen.scale then
	--	 keypress.menu('escape')
	-- end
	if ram.wait == 0 then
		x = (x - screen.offset.x) / screen.scale
		y = (y - screen.offset.y) / screen.scale

		if ram.option == 'settings' then
			-- Left arrows
			if x > 0 and x < 32 then
				for i = 1, math.min(#ram.settings, 5) do
					if
						y > i * 48 - 48
					and
						y < i * 48
					then
						local p = ram.option
						ram.settings[i].selected = (ram.settings[i].selected - 2) % #ram.settings[i].options + 1
						ram.settings[i].options[ram.settings[i].selected].func()
						-- Move the cursor to where you clicked, but only if the menu didn't change
						if p == ram.option then
							ram.cursor = i
						end
					end
				end
			-- Right arrows
			elseif x > 128 and x < 160 then
				for i = 1, math.min(#ram.settings, 5) do
					if
						y > i * 48 - 48
					and
						y < i * 48
					then
						local p = ram.option
						ram.settings[i].selected = ram.settings[i].selected % #ram.settings[i].options + 1
						ram.settings[i].options[ram.settings[i].selected].func()
						if p == ram.option then
							ram.cursor = i
						end
					end
				end
			elseif x > 160 and x < 192 then
				for i = 1, #ram.settings - 5 do
					if
						y > i * 48 - 48
					and
						y < i * 48
					then
						local i = i + 5
						local p = ram.option
						ram.settings[i].selected = (ram.settings[i].selected - 2) % #ram.settings[i].options + 1
						ram.settings[i].options[ram.settings[i].selected].func()
						if p == ram.option then
							ram.cursor = i
						end
					end
				end
			elseif x > 288 and x < 320 then
				for i = 1, #ram.settings - 5 do
					if
						y > i * 48 - 48
					and
						y < i * 48
					then
						local i = i + 5
						local p = ram.option
						ram.settings[i].selected = ram.settings[i].selected % #ram.settings[i].options + 1
						ram.settings[i].options[ram.settings[i].selected].func()
						if p == ram.option then
							ram.cursor = i
						end
					end
				end
			end

		elseif ram.option == 'scores' then
			if x > 0 and x < 79 then
				if y > 220 and y < 234 then
					ram.cursor = 1
					ram.option = 'main'
				end
			end
		elseif ram.option == 'areyousure' then
			if x > 128 and x < 192 then
				if y > 144 and y < 172 then
					ram.cursor = math.floor((y - 144)/16) + 1
					ram[ram.option][ram.cursor].func()
				end
			end
		elseif ram.option == 'main' then
			-- Main menu
			if x > 96 and x < 224 then
				if y > 106 and y < #ram[ram.option] * 16 + 106 then
					ram.cursor = math.ceil((y-106)/16)
					ram[ram.option][ram.cursor].func()
					return
				end
			end
		else
			-- Settings menu
			if x > 94 and x < 166 then
				if y > 72 and y < #ram[ram.option] * 16 + 72 then
					ram.cursor = math.ceil((y-72)/16)
					ram[ram.option][ram.cursor].func()
					return
				end
			end
			-- Controller config
			-- Buttons to reset controls
			if ram.option == 'controller' then
				if x > 212 and x < 228 then
					if y > 72 and y < #ram[ram.option] * 16 + 72 then
						save.controller[math.ceil((y-72)/16)].button = 'none'
					end
				end
			end
		end
	end
end