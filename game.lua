function init.game(flashlevel)
	setMusicVolume(0.2)
	-- If it's a new game...
	if not playing then
		gram = {
			state = 'game',
			pause = 0, -- pretty much only used for game over
			grid = {},
			held = {
				gem = {math.random(1,4), math.random(1,4), math.random(1,4)},
				x = 3,
				y = -1,
			},
			-- "next" is a reserved word in Lua - this is the next/preview peice
			nex = {
				math.random(1,4),
				math.random(1,4),
				math.random(1,4)
			},
			-- Drops when reaches 0
			drop = 1,
			-- 16 means paused because it might land exactly on 0 seconds when ticking down
			bursttimer = 16,
			falltimer = 16, -- Timer for dropping gems
			fallspeed = 0, -- Speed of gems when they drop after a burst
			shift = 0,	-- Timer for rotating gems
			tally = 0,	-- Shows score of last combo
			score = 0,
			level = 1,
			flashlevel = 1,
			flashtime = 0,
			tallyalpha = 0,
			combo = 0,
			magic = 0, -- when it reaches 64, a magic gem can appear
			twinkle = 0, -- Timer for when to spawn magic gem sparkles
			stars = {},
			levels = {
				0.5,
				0.75,
				1,
				1.25,
				2.5,
				1,
				1.5,
				2,
				2.5,
				5,
				1.5,
				2.25,
				3,
				3.75,
				7.5,
				2,
				3,
				4,
				5,
				10,
			}
		}
		-- Make an empty grid
		for x = 1,6 do
			gram.grid[x] = {}
			for y = -2,13 do
				gram.grid[x][y] = {
					gem = 0,
					burst = false,
					falling = false,
				}
			end
		end
		playing = true
		if save.mode == 'flash' then
			if flashlevel then
				gram.flashlevel = flashlevel
			end
			gram.level = math.floor(gram.flashlevel*0.25) + 1
			love.math.setRandomSeed(gram.flashlevel + 1820695)
			-- Generate a random board first
			for x = 1, 6 do
				for y = 11 - math.floor(math.min((save.difficulty*gram.flashlevel)*0.125, 8)), 13 do
					gram.grid[x][y] = {
						gem = math.random(1, 3 + save.difficulty),
						burst = false,
						falling = false
					}
				end
			end
			-- Try at most 64 times to randomly replace bursted gems until none match
			for i = 1,64 do
				-- Makes sure no peices make a line clear
				if not check(true) then
					break
				else
					for x = 1, 6 do
						for y = 1,13 do
							if gram.grid[x][y].burst then
								gram.grid[x][y] = {
									gem = math.random(1, 3 + save.difficulty),
									burst = false,
									falling = false
								}
							else
								gram.grid[x][y].burst = false
							end
						end
					end
				end
			end
			-- Defines which crystal is the flash crystal
			gram.flash = {x = math.random(1, 6), y = 13}
		end
	end
end

function update.game(dt)
	if gram.pause > 0 then
		gram.pause = gram.pause - dt
		if gram.pause <= 0 then
			gram.pause = 0
			gram.state = gram.nextState
		end
	end

	--If not on the game over screen...
	if gram.state == 'game' then
		if gram.bursttimer == 16 and gram.falltimer == 16 then

			-- Update timers
			-- Updates crystals flashing upon clear
			if gram.flashtime and gram.pause == 0 then
				gram.flashtime = gram.flashtime + dt
			end
			if gram.twinkle > 0 then
				gram.twinkle = gram.twinkle - dt*256
			end
			-- Drop held crystal
			if gram.pause == 0 then
				gram.drop = gram.drop - dt * gram.levels[gram.level]*(1 + save.difficulty * 0.5)
				if gram.shift > 0 then
					gram.shift = gram.shift - dt * 128
				else
					gram.shift = 0
				end
			end

			-- If it's a magic gem...
			-- Generate new stars
			if gram.held.gem[1] == 7 then
				local c = gram.held
				local p = gram.grid[c.x][c.y]
				if gram.twinkle <= 0 then
					table.insert(gram.stars, {96 + c.x*16 + math.random(0, 16), 16 + c.y*16 + math.random(-8, 40), 4})
					gram.twinkle = 5 + math.random(1,3)
				end
			end

			-- Remove old ones
			for k, v in pairs(gram.stars) do
				v[3] = v[3] - dt * 8
				if v[3] < 0 then
					table.remove(gram.stars, k)
				end
			end

			control.update(dt)

			if gram.drop <= 0 and gram.held then
				if gram.held.y < 11 and not checkCol(0,1) then
					gram.drop = 1
					gram.held.y = gram.held.y + 1
				else
					-- Place crystal
					playSound('place')


					-- If it's a magic gem...
					local c = gram.held
					if c then
						if c.gem[1] == 7 then
							if c.y > 10 then
								-- Adds 10,000 points for an unused magic gem
								gram.tallyalpha = 255
								gram.tally = 10000
								gram.score = gram.score + gram.tally
							else
								local gem = gram.grid[c.x][c.y + 3].gem
								for x = 1,6 do
									for y = 1,13 do
										local c = true
										if save.mode == 'flash' then
											if x == gram.flash.x and y == gram.flash.y then
												c = false
											end
										end
										if gram.grid[x][y].gem == gem and c then
											gram.grid[x][y].burst = true
										end
									end
								end
							end
							for i = 0,2 do
								gram.grid[c.x][c.y+i].burst = true
							end
						end
					end

					for i = 0,2 do
						gram.grid[gram.held.x][gram.held.y + i].gem = gram.held.gem[i+1]
					end
					gram.held = nil
					-- Check for line clears, if there are none, generate the new peice
					if not check() then
						generate()
					end
				end
			end
		elseif gram.bursttimer < 0 then
			gram.bursttimer = 16
			gram.falltimer = 0
		elseif gram.falltimer <= 0 then
			local tally = 0
			-- Offset - appends particles to existing table
			local ofs = 0
			if not gram.particle then
				gram.particle = {}
			else
				ofs = #gram.particle
			end

			-- Clear bursted gems
			for x = 1,6 do
				for y = 13,-1,-1 do
					if gram.grid[x][y].burst then
						-- Add particles
						gram.particle[tally + 1 + ofs] = {
							{
								x = x * 16 + 96,
								y = y * 16,
								vx = -0.2 + math.random(1,16)/64,
								vy = -1 + math.random(1,16)/64,
							},
							{
								x = x * 16 + 96,
								y = y * 16,
								vx = 0.2 + math.random(1,16)/64,
								vy = -1 + math.random(1,16)/64,
							},
							{
								x = x * 16 + 96,
								y = y * 16,
								vx = -0.2 + math.random(1,16)/64,
								vy = -0.8 + math.random(1,16)/64,
							},
							{
								x = x * 16 + 96,
								y = y * 16,
								vx = 0.2 + math.random(1,16)/64,
								vy = -0.8 + math.random(1,16)/64,
							},
							gram.grid[x][y].gem,
						}
						tally = tally + 1
						gram.grid[x][y] = {
							gem = 0,
							burst = false,
							falling = false
						}
					end
				end
			end


			--Find what gems need to fall
			local f = false
			for x = 1,6 do
				for y = 13, -1, -1 do
					-- If the currently selected gem should be falling right now...
					if
						gram.grid[x][y - 1].gem ~= 0
					and
						gram.grid[x][y].gem == 0
					then
						-- Drop it immediately, and tell the draw call to draw it above where it actually is and smoothly slide it down
						gram.grid[x][y].gem = gram.grid[x][y - 1].gem
						gram.grid[x][y - 1] = {
							gem = 0,
							burst = false,
							falling = false
						}
						gram.grid[x][y].falling = true
						f = true
					else
						gram.grid[x][y].falling = false
					end
				end
			end

			-- Calculate score and play sound
			if tally > 0 then
				-- Make sure pause timer goes back up if you clear another line
				if gram.pause > 0 then
					gram.pause = 5
				end
				sfx.burst:setPitch(0.95 + math.random(0,10) * 0.01)
				playSound('burst')
				gram.tallyalpha = 255
				gram.tally = tally * 10 * (2 ^ gram.combo + 1) * (save.difficulty*save.difficulty)
				gram.score = gram.score + gram.tally
				local currentLevel = gram.level
				if save.mode == 'flash' then
					gram.level = math.floor(gram.flashlevel*0.25) + 1
				else
					gram.level = math.min(math.ceil(gram.score/10000), 20)
				end
				if gram.level > currentLevel and save.mode ~= 'flash' then
					playSound('lvlup')
				end
			end
			-- If no gems were cleared...
			if not f then
				gram.falltimer = 16
				gram.fallspeed = 0
				-- If there is no more bursting to be done, generate a new piece.
				if not check() then
					generate()
					gram.combo = 0
				end
			else
				gram.falltimer = 1
				gram.fallspeed = gram.fallspeed + 4
				if tally > 1 then
					gram.combo = gram.combo + 1
				end
			end

		--Update timers
		elseif gram.falltimer ~= 16 then
			gram.falltimer = gram.falltimer - dt*(2 + gram.fallspeed)
		elseif gram.bursttimer ~= 16 then
			gram.bursttimer = gram.bursttimer - dt
		end
	elseif gram.state == 'gameover' then
		playing = false
		stopSound()
		setstate('menu', true)
		return
	elseif gram.state == 'gameoverhigh' then
		gram.state = 'name'
		playMusic('nameentry')
	elseif gram.state == 'flashlevelup' then
		local a = gram.flashlevel + 1
		local b = gram.flashtime
		gram = nil
		playing = false
		init.game(a)
		gram.flashlevel = a
		gram.flashtime = b
	elseif gram.state == 'name' then
		control.update(dt)
		local place = gram.name.place
		local cursor = gram.name.cursor
		for i = 1,3 do
			if math.abs(place[i] - cursor[i]) > 0.1 then
				cursor[i] = cursor[i] - (cursor[i]-place[i])*dt*8
			else
				place[i] = place[i]%26
				cursor[i] = place[i]
			end
		end
		gram.name.place = place
		gram.name.cursor = cursor
	end

	-- Update particle system
	if gram.particle then
		for i = 1, #gram.particle do
			for j = 1,4 do
				local c = gram.particle[i][j]
				c.vy = c.vy + dt*2
				c.x = c.x + c.vx*dt*256
				c.y = c.y + c.vy*dt*256
			end
		end
	end

	-- Update live BG
	if save.livebg then
		bgcode.update(dt)
	end

	if gram.tallyalpha > 0 then
		gram.tallyalpha = gram.tallyalpha - (dt*240)
	end
	if gram.tallyalpha < 0 then gram.tallyalpha = 0 end
end

function draw.game()
	-- Draw background
	if save.livebg then
		bgcode.draw()
	else
		local scale = math.ceil(math.max(screen.w, screen.h)/512)
		love.graphics.draw(tex.bg[save.bg], screen.w * 0.5 - scale * 256, screen.h * 0.5 - scale * 256, 0, scale)
	end
	-- Start 320x240 graphics
	love.graphics.setCanvas(canvas[1])

	-- Draw backdrop
	love.graphics.draw(tex.gamegrid, 0, 0)

		-- Draw grid
	love.graphics.setScissor(112, 16, 96, 208)

	for x = 1,6 do
		for y = -1,13 do
			local gem = gram.grid[x][y].gem
			if gem ~= 0 then
				if gram.bursttimer ~= 16 then
					if gram.grid[x][y].burst and (gram.bursttimer*16)%1 > 0.5 then
						love.graphics.draw(tex.gems[save.gem], quad.flash[gem], x * 16 + 95, (y+2) * 16 - 32)
					else
						love.graphics.draw(tex.gems[save.gem], quad.gem[gem], x * 16 + 96, (y+2) * 16 - 32)
					end
				else
					local f = gram.grid[x][y].falling
					if f then
						love.graphics.draw(tex.gems[save.gem], quad.gem[gem], x * 16 + 96, (y + 2 - gram.falltimer) * 16 - 32)
					else
						love.graphics.draw(tex.gems[save.gem], quad.gem[gem], x * 16 + 96, (y+2) * 16 - 32)
					end
				end
			end
			if save.mode == 'flash' then
			-- gram.pause == 0 so the gem doesn't flash during the "next level" transition, since it can be a different gem after you clear it
				if x == gram.flash.x and y == gram.flash.y and gram.grid[x][y].gem ~= 0 and gram.pause == 0 then
					-- Flash gem
					love.graphics.setBlendMode('add')
					love.graphics.setColor(math.random(1,255)/255,math.random(1,255)/255,math.random(1,255)/255)
					love.graphics.draw(tex.gems[save.gem], quad.gem[gram.grid[x][y].gem], (x * 16 + 96) + math.random(-1, 1), ((y+2) * 16 - 32) + math.random(-1, 1))
					love.graphics.setColor(1,1,1,1)
					love.graphics.setBlendMode('alpha')
				end
			end
		end
	end

	-- Draw held crystal
	if gram.held then
		local h = gram.held
		if h.gem[1] == 7 then
			for i = 1,3 do
				love.graphics.draw(tex.gems[save.gem], quad.gem[h.gem[i]], h.x * 16 + 96, h.y*16 - 16 + i*16 + gram.shift*2)
			end
		else
			local offset = math.floor(2 - gram.drop*2) * 8 + 8
			love.graphics.draw(tex.gems[save.gem], quad.gem[h.gem[1]], h.x * 16 + 96, h.y*16 - 16 + offset + gram.shift*2)
			love.graphics.draw(tex.gems[save.gem], quad.gem[h.gem[2]], h.x * 16 + 96, h.y*16			+ offset - gram.shift)
			love.graphics.draw(tex.gems[save.gem], quad.gem[h.gem[3]], h.x * 16 + 96, h.y*16 + 16 + offset - gram.shift)
		end
	end

	-- Draw particle system
	if gram.particle then
		local ass = false
		for i = 1, #gram.particle do
			for j = 1,4 do
				local c1 = gram.particle[i]
				local c2 = gram.particle[i][j]
				if c2.y < 240 then
					love.graphics.draw(tex.gems[save.gem], quad.particle[c1[5]][j - 1], c2.x, c2.y, 0, 1, 1, 4, 4)
					ass = true
				end
			end
		end
		if not ass then
			gram.particle = {}
		end
	end

	-- Draw magic gem sparkles
	for k, v in pairs(gram.stars) do
		love.graphics.setColor(
			math.random(100, 255)/255,
			math.random(100, 255)/255,
			math.random(100, 255)/255,
			math.random(100, 255)/255
		)
		if quad.twinkle[math.floor(v[3])] then
			love.graphics.draw(tex.twinkle, quad.twinkle[math.floor(v[3])], v[1], v[2], 0, 1, 1, 4, 4)
		end
		love.graphics.setColor(1,1,1,1)
	end


	love.graphics.setScissor()
	-- Draw flash columns info
	if save.mode == 'flash' then
	local a, b = getTime(gram.flashtime)
		drawText(a, 63, 145)
		drawText(b, 63, 157)
		drawText('flash level: ' .. gram.flashlevel, 103, 3)
	end


	-- Draw next crystal
	if gram.nex then
		for i = 1,3 do
			local gem = gram.nex[i]
			if gem ~= 0 then
				love.graphics.draw(tex.gems[save.gem], quad.gem[gram.nex[i]], 88, i*16)
			end
		end
	end

	-- Draw score
	if save.mode ~= 'flash' then
		love.graphics.setColor(0.8, 0.8, 0.8, gram.tallyalpha)
		drawText(gram.tally, 105, 73, 'right')
		love.graphics.setColor(0.7, 0.7, 0.7, 1)

		drawText(gram.score, 105, 89, 'right')
		drawText('score', 88, 101, 'right')
		love.graphics.setColor(1,1,1,1)
	end

	-- Draw level
	love.graphics.setColor(0.8, 0.8, 0.8, 1)
	drawText(gram.level, 105, 117,'right')
	drawText('level', 88, 129, 'right')
	love.graphics.setColor(1,1,1,1)

	-- Draw game over notice
	if gram.nextState == 'gameover' then
		love.graphics.setColor(0, 0, 0, 0.5)
		love.graphics.rectangle('fill', 110, 14, 100, 212)
		love.graphics.setColor(1,1,1,1)
		drawText("game over", 120, 112)
		love.graphics.setColor(1,1,1, 1/4)
		love.graphics.rectangle('fill', 112, 128, gram.pause * 19.2, 8)
		love.graphics.setColor(1,1,1,1)
	elseif gram.nextState == 'gameoverhigh' then
		love.graphics.setColor(0, 0, 0, 0.5)
		love.graphics.rectangle('fill', 110, 14, 100, 212)
		love.graphics.setColor(1,1,1,1)
		drawText("game over", 120, 112)
		drawText("new high", 125, 64)
		drawText("score", 138, 80)
		love.graphics.setColor(1,1,1, 1/4)
		love.graphics.rectangle('fill', 112, 128, gram.pause * 19.2, 8)
		love.graphics.setColor(1,1,1,1)
	elseif gram.nextState == 'flashlevelup' then
		love.graphics.setColor(0, 0, 0, 0.5)
		love.graphics.rectangle('fill', 110, 14, 100, 212)
		love.graphics.setColor(1,1,1,1)
		drawText("level up", 120, 112)
		if gram.score > save.scores[save.mode][save.difficulty].score then
			drawText("on to", 125, 64)
			drawText("level " .. gram.flashlevel + 1, 138, 80)
		end
		love.graphics.setColor(1,1,1, 1/4)
		love.graphics.rectangle('fill', 112, 128, gram.pause * 19.2, 8)
		love.graphics.setColor(1,1,1,1)
	end

	if gram.state == 'name' then
		love.graphics.setColor(0, 0, 0, 0.8)
		love.graphics.rectangle('fill', 110, 14, 100, 212)
		love.graphics.setColor(1,1,1,1)
		drawText('enter your', 115, 64)
		drawText('name', 140, 80)

		local place = gram.name.place
		local cursor = gram.name.cursor
		local tic = gram.name.tic

		--drawText('use arrow keys or type', 0, 0, 0, 2)
		-- Text circle, name entry

		for x = 3, 1, -1 do
			for y = 1,26 do
				local s = math.sin( ( (y - cursor[x]) *3.1415 ) / 13)
				local h = math.abs(math.sin( ( (y - cursor[x] - 13) *3.1415 ) / 26)*2)
				love.graphics.setColor(h*.5,h*.5,h*.5)
				drawText(string.char(y + 96),128 + h * 8 + (x-1) * 16, 160 - s*64, 'left', h*0.75)
			end
		end
		-- Name wheel cursor
		love.graphics.setColor(.5, 3/4, 1, .5)
		love.graphics.rectangle('fill', 128 + tic * 16, 160, 16, 16)
		love.graphics.setColor(1,1,1,1)

		--Name wheel arrows
		love.graphics.setColor(0,0,0,3/4)
		love.graphics.rectangle('fill', 144, 144, 48, 16)
		love.graphics.rectangle('fill', 144, 176, 48, 16)
		love.graphics.setColor(1,1,1,1)

		love.graphics.draw(tex.arrow, 160, 144, math.pi/2)
		love.graphics.draw(tex.arrow, 176, 144, math.pi/2)
		love.graphics.draw(tex.arrow, 192, 144, math.pi/2)

		love.graphics.draw(tex.arrow, 144, 192, -math.pi/2)
		love.graphics.draw(tex.arrow, 160, 192, -math.pi/2)
		love.graphics.draw(tex.arrow, 176, 192, -math.pi/2)

		love.graphics.draw(tex.check, 192, 160)

		gram.name.place = place
		gram.name.cursor = cursor
		gram.name.tic = tic
	end
	-- End 320x240 graphics
	love.graphics.setCanvas(canvas[2])
	love.graphics.draw(canvas[1], 0, 0, 0, 4)
	love.graphics.setCanvas()
	love.graphics.draw(canvas[2], screen.offset.x, screen.offset.y, 0, screen.scale/4)

	-- Clear canvases
	love.graphics.setCanvas(canvas[1])
	love.graphics.clear()
	love.graphics.setCanvas(canvas[2])
	love.graphics.clear()
	love.graphics.setCanvas()

	control.draw()
end



-- Check collision of held crystal and grid
function checkCol(ox, oy)
	for i = 0,2 do
		local x, y = gram.held.x + ox, gram.held.y + oy + i
		if x > 0 and x < 7 and y > 0 and y < 14 then
			if gram.grid[x][y].gem ~= 0 then
				return true
			end
		end
	end
	return false
end


-- This function gets grid values, but returns blank table instead of crashing when out-of-bounds
function getgem(x, y)
	if x < 1 or x > 6 or y < -1 or y > 13 then
		return {gem = 0, burst = false}
	else
		return gram.grid[x][y]
	end
end

-- Check for line clears
function check(test)
	local counter = 1
	local burst = false
	-- horizontal check
	for y = -1, 13 do
		counter = 1
		for x = 1, 6 do
			if getgem(x, y).burst then burst = true end -- check for magic gem clears
			if (getgem(x,y)).gem == (getgem(x+1,y)).gem
			and (getgem(x,y)).gem ~= 0
			then
				counter = counter + 1
			else
				if counter >= 3 then
					for i = 1, counter do
						getgem(x - counter + i,y).burst = true
					end
					burst = true
				end
				counter = 1
			end
		end
	end
	-- vertical check
	for x = 1, 6 do
		counter = 1
		for y = -1, 13 do
			if (getgem(x,y)).gem == (getgem(x, y+1)).gem
			and (getgem(x,y)).gem ~= 0
			then
				counter = counter + 1
			else
				if counter >= 3 then
					for i = 1, counter do
						getgem(x,y - counter + i).burst = true
					end
					burst = true
				end
				counter = 1
			end
		end
	end
	-- diagonal \ check
	local lastx = 1
	counter = 1
	--for i = 0, 77 do
	for i = -12, 77 do
		local x = i%6 + 1
		local y = i%13 + 1
		if x ~= lastx + 1 then
			counter = 1
		end
		lastx = x
		if (getgem(x,y)).gem == (getgem(x + 1,y + 1)).gem
		and (getgem(x,y)).gem ~= 0
		then
			counter = counter + 1
		else
			if counter >= 3
			and (getgem(x,y)).gem ~= (getgem(x + 1,y + 1)).gem
			then
				for i = 1, counter do
					(getgem(x - counter + i,y - counter + i)).burst = true
					burst = true
				end
			end
			counter = 1
		end
	end
	-- diagonal / check
	local lastx = 1
	counter = 1
	--for i = 0, 77 do
	for i = -12, 77 do
		local x = (5 - i%6) + 1
		local y = i%13 + 1
		if x ~= lastx - 1 then
			counter = 1
		end
		lastx = x
		if (getgem(x,y)).gem == (getgem(x - 1,y + 1)).gem
		and (getgem(x,y)).gem ~= 0
		then
			counter = counter + 1
		else
			if counter >= 3
			and (getgem(x,y)).gem ~= (getgem(x - 1,y + 1)).gem
			then
				for i = 1, counter do
					(getgem(x + counter - i,y - counter + i)).burst = true
					burst = true
				end
			end
			counter = 1
		end
	end

	if burst then
		if test then
			return true
		else
			if gram.flash then
				-- Flash columns completed
				if gram.grid[gram.flash.x][gram.flash.y].burst then
					gram.nextState = 'flashlevelup'
					gram.pause = 5
				end
			end
			gram.bursttimer = 0.5
			return true
		end
	end
end

-- Generates new crystal
function generate()
	gram.held = {
		gem = gram.nex,
		x = 3,
		y = -1
	}
	if math.random(1,4) == 1 and gram.magic > 64*save.difficulty then
		-- Generate magic column
		gram.magic = 0
		gram.nex = {7,8,9}
	else
		gram.nex = {
			math.random(1,3 + save.difficulty),
			math.random(1,3 + save.difficulty),
			math.random(1,3 + save.difficulty)
		}
		gram.magic = gram.magic + 1
	end
	gram.drop = 1
	if checkCol(0,0) then
		gameover()
	end
end