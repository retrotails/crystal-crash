return {
	init = function()
		bgdata = {
			sunset = love.image.newImageData(1, 2),
			t = 0,
			colors = {
				{
					{1, 0.5, 3/4},	-- Top horizon color
					{1, 0.5, 0.5},	-- Bottom horizon color
					{1, 3/4, 3/4},	-- Scene color
				},
				{
					{0, 0.5, 3/4},
					{1/4, 3/4, 1},
					{1, 1, 1},
				},
				{
					{0, 1/4, 3/8},
					{0.5, 1/4, 0.5},
					{1/4, 0.5, 3/4},
				},
				{
					{0, 0, 0},
					{0, 0, 0},
					{1/4, 1/4, 1/4},
				},
			},
			mix = function(index, alpha)
				local tab1 = bgdata.colors[math.floor(bgdata.t+1)][index]
				local tab2 = bgdata.colors[math.floor(bgdata.t+1)%4+1][index]
				local tab3 = {}
				for i = 1,3 do
					tab3[i] = tab1[i] + (tab2[i] - tab1[i]) * (bgdata.t%1)
				end
				if alpha then tab3[4] = alpha else tab3[4] = 1 end
				return tab3
			end,
		}
		bgdata.sunset2 = love.graphics.newImage(bgdata.sunset)
		bgdata.sunset2:setFilter('linear','linear')
		bgcode.resize(love.graphics.getWidth(), love.graphics.getHeight())
	end,
	update = function(dt)
		bgdata.t = (bgdata.t + dt/32)%4
	end,
	draw = function()
		love.graphics.push()
		bgdata.sunset:setPixel(0, 0, unpack(bgdata.mix(1)))
		bgdata.sunset:setPixel(0, 1, unpack(bgdata.mix(2)))
		bgdata.sunset2 = love.graphics.newImage(bgdata.sunset)
		bgdata.sunset2:setFilter('linear','linear')
		love.graphics.scale(bgdata.scale)
		love.graphics.draw(bgdata.sunset2, 0, 0, 0, 360, 64)
		if shader then
			shader.effect.sky:send('t', bgdata.t*4)
			love.graphics.setShader(shader.effect.sky)
			love.graphics.draw(tex.livebg.clouds, 0, 0, 0, 1)
			love.graphics.setShader()
		end
		love.graphics.setColor(unpack(bgdata.mix(3, 0.5)))
		love.graphics.draw(bgdata.sunset2, 0, 0, 0, 360, 64)
		love.graphics.setColor(unpack(bgdata.mix(3, 1)))
		if bgdata.t > 0 and bgdata.t <= 1 then
			love.graphics.circle('fill', 1024, 512 - bgdata.t * 768, 128, 32)
		elseif bgdata.t > 1 and bgdata.t <= 2 then
		elseif bgdata.t > 2 and bgdata.t <= 3 then
		elseif bgdata.t > 3 then
		end
		love.graphics.draw(tex.livebg.ocean, 96 + math.cos(bgdata.t*6.189) * 64, 80, 0, 1.5, .5)
		love.graphics.draw(tex.livebg.acropolis)
		love.graphics.pop()
		love.graphics.setColor(1,1,1)
	end,
	resize = function(w, h)
		bgdata.scale = math.max(w/360, h/240)
	end,
}