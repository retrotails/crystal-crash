-- Draws touch controls
control = {}
function control.draw()
	love.graphics.setColor(1,1,1, 150/255)
	-- If the touch buttons are enabled...
	for i, v in ipairs(touch.coord) do
		love.graphics.draw(tex.buttons, quad.touch[touch.coord[i].quad], touch.coord[i].x, touch.coord[i].y, 0, screen.scale * (save.touchscale/32))
	end
	love.graphics.setColor(1,1,1,1)
end

function control.update(dt, menu) -- menu is for the main menu, where emulator-style touch controls are disabled
	if not menu then
		--DAS stuff
		for i, k in pairs(das) do
			if isDown(i) then
				das[i] = das[i] - dt*2*(1 + 0.1 * gram.level)
				if das[i] <= 0 then
					das[i] = .1
					love.keypressed(i)
				end
			else
				das[i] = .5
			end
		end
		-- Prevents issues caused by holding left+right at the same time
		if das.right < 0.5 and das.left < 0.5 then
			if das.right > das.left then
				das.left = 0.5
			else
				das.right = 0.5
			end
		end

		-- Update mouse positions
		mouse.last.x = mouse.pos.x
		mouse.last.y = mouse.pos.y
		mouse.pos.x = (love.mouse.getX() - screen.offset.x)/screen.scale
		mouse.pos.y = (love.mouse.getY() - screen.offset.y)/screen.scale

		-- Swipe controls
		if save.touch == 3 then
			if love.mouse.isDown(1) and mouse.first then
				mouse.touchtime = mouse.touchtime + dt
				if gram.held.x < mouse.crys.x + math.floor((mouse.pos.x - mouse.first.x)/32) then
					love.keypressed('right')
				elseif gram.held.x > mouse.crys.x + math.floor((mouse.pos.x - mouse.first.x)/32) then
					love.keypressed('left')
				end
				if math.abs(mouse.first.y - mouse.pos.y) > math.abs(mouse.first.x - mouse.pos.x) then
					if gram.held.y < mouse.crys.y + math.floor((mouse.pos.y - mouse.first.y)/16) then
						love.keypressed('down')
					end
				end
			else
				mouse.touchtime = 0
			end
		elseif save.touch == 4 then
			if love.mouse.isDown(1) and mouse.first then
				mouse.touchtime = mouse.touchtime + dt
				if gram.held.x < mouse.crys.x + math.floor((mouse.pos.x - mouse.first.x)/32) then
					love.keypressed('right')
				elseif gram.held.x > mouse.crys.x + math.floor((mouse.pos.x - mouse.first.x)/32) then
					love.keypressed('left')
				end
			else
				mouse.touchtime = 0
			end
		end
	end

	-- Update controller analog positions as buttons
	if controller.count > 0 then
		--Check sticks
		local count = controller.object[save.stick]:getAxisCount()
		for i = 1, count, 2 do
			local direction = controller.object[save.stick]:getAxis(i)
			if direction < -controller.deadzone then
				if not controller.lastheld['left ' .. i] then
					love.joystickpressed(save.stick, 'left ' .. i)
					controller.lastheld['left ' .. i] = true
				end
				controller.held['left ' .. i] = true
			else
				controller.lastheld['left ' .. i] = false
				controller.held['left ' .. i] = false
			end
			if direction > controller.deadzone then
				if not controller.lastheld['right ' .. i] then
					love.joystickpressed(save.stick, 'right ' .. i)
					controller.lastheld['right ' .. i] = true
				end
				controller.held['right ' .. i] = true
			else
				controller.lastheld['right ' .. i] = false
				controller.held['right ' .. i] = false
			end
			local direction = controller.object[save.stick]:getAxis(i + 1)
			if direction < -controller.deadzone then
				if not controller.lastheld['up ' .. i] then
					love.joystickpressed(save.stick, 'up ' .. i)
					controller.lastheld['up ' .. i] = true
				end
				controller.held['up ' .. i] = true
			else
				controller.lastheld['up ' .. i] = false
				controller.held['up ' .. i] = false
			end
			if direction > controller.deadzone then
				if not controller.lastheld['down ' .. i] then
					love.joystickpressed(save.stick, 'down ' .. i)
					controller.lastheld['down ' .. i] = true
				end
				controller.held['down ' .. i] = true
			else
				controller.lastheld['down ' .. i] = false
				controller.held['down ' .. i] = false
			end
		end
		--Check hats
		for i = 1, controller.object[save.stick]:getHatCount() do
			local direction = controller.object[save.stick]:getHat(i)
			if direction == 'l' then
				if not controller.lastheld['left hat ' .. i] then
					love.joystickpressed(save.stick, 'left hat ' .. i)
					controller.lastheld['left hat ' .. i] = true
				end
				controller.held['left hat ' .. i] = true
			else
				controller.lastheld['left hat ' .. i] = false
				controller.held['left hat ' .. i] = false
			end
			if direction == 'r' then
				if not controller.lastheld['right hat ' .. i] then
					love.joystickpressed(save.stick, 'right hat ' .. i)
					controller.lastheld['right hat ' .. i] = true
				end
				controller.held['right hat ' .. i] = true
			else
				controller.lastheld['right hat ' .. i] = false
				controller.held['right hat ' .. i] = false
			end
			if direction == 'u' then
				if not controller.lastheld['up hat ' .. i] then
					love.joystickpressed(save.stick, 'up hat ' .. i)
					controller.lastheld['up hat ' .. i] = true
				end
				controller.held['up hat ' .. i] = true
			else
				controller.lastheld['up hat ' .. i] = false
				controller.held['up hat ' .. i] = false
			end
			if direction == 'd' then
				if not controller.lastheld['down hat ' .. i] then
					love.joystickpressed(save.stick, 'down hat ' .. i)
					controller.lastheld['down hat ' .. i] = true
				end
				controller.held['down hat ' .. i] = true
			else
				controller.lastheld['down hat ' .. i] = false
				controller.held['down hat ' .. i] = false
			end
		end
	end
end

function mousepress.game(x, y, button)
	mouse.last.x = mouse.pos.x
	mouse.last.y = mouse.pos.y
	mouse.pos.x = (love.mouse.getX() - screen.offset.x)/screen.scale
	mouse.pos.y = (love.mouse.getY() - screen.offset.y)/screen.scale
	if gram.state == 'game' or gram.state == 'name' then
		if save.touch == 3 or save.touch == 4 then
			-- Swipe mode
			if not mouse.first then
				mouse.first= {
					x = mouse.pos.x,
					y = mouse.pos.y
				}
				mouse.crys = {
					x = gram.held.x,
					y = gram.held.y,
				}
			end
		else
			-- Touch buttons
			for i, v in ipairs(touch.coord) do
				if control.getTouch(x, y, i) then
					love.keypressed(touch.coord[i].key)
				end
			end
		end
	elseif gram.state == 'name' then
		-- Name wheel buttons
		x = (x - screen.offset.x)/screen.scale
		y = (y - screen.offset.y)/screen.scale
		-- Top row
		if y > 144 and y < 160 then
			if x > 144 and x < 160 then
				gram.name.tic = 1
				gram.name.place[1] = gram.name.place[1] - 1
			elseif x > 160 and x < 176 then
				gram.name.place[2] = gram.name.place[2] - 1
			elseif x > 176 and x < 192 then
				gram.name.place[3] = gram.name.place[3] - 1
			end
		-- Bottom row
		elseif y > 176 and y < 192 then
			if x > 144 and x < 160 then
				gram.name.tic = 1
				gram.name.place[1] = gram.name.place[1] + 1
			elseif x > 160 and x < 176 then
				gram.name.place[2] = gram.name.place[2] + 1
			elseif x > 176 and x < 192 then
				gram.name.place[3] = gram.name.place[3] + 1
			end
		-- "done" button
		elseif x > 192 and x < 208 and y > 160 and y < 176 then
			scorepush()
			playing = false
			stopSound()
			setstate('menu', true)
			return
		end
	end
end

function mouserelease.game()
	mouse.last.x = mouse.pos.x
	mouse.last.y = mouse.pos.y
	mouse.pos.x = (love.mouse.getX() - screen.offset.x)/screen.scale
	mouse.pos.y = (love.mouse.getY() - screen.offset.y)/screen.scale
	if mouse.first then
		if	math.abs(mouse.first.x - mouse.pos.x) < 16
		and math.abs(mouse.first.y - mouse.pos.y) < 16
		and mouse.touchtime < 0.2
		then
			love.keypressed(' ')
		end
		mouse.first = nil
		mouse.down = 0
		mouse.touchtime = 0
	end
end

function keypress.game(key)
	-- If the burst animation isn't playing...
	if gram.bursttimer == 16 and gram.falltimer == 16 and gram.state == 'game' and gram.pause == 0 then
		if key == 'left' and not checkCol(-1, 0) then
			gram.held.x = math.max(gram.held.x - 1, 1)
		elseif key == 'right' and not checkCol(1, 0) then
			gram.held.x = math.min(gram.held.x + 1, 6)
		elseif key == 'down' then
			gram.drop = 0
		elseif key == 'up' then
			for y = 13,1,-1 do
				if gram.grid[gram.held.x][y].gem == 0 then
					gram.held.y = y-2
					gram.drop = 0
					return
				end
			end
		elseif key == ' ' or key == 'return' then
			if gram.held.gem[1] ~= 7 then
				gram.shift = 16
				playSound('rotate')
				gram.held.gem[1],
				gram.held.gem[2],
				gram.held.gem[3]
				=
				gram.held.gem[3],
				gram.held.gem[1],
				gram.held.gem[2]
			end
		elseif key == 'escape' then
			setstate('menu', true)
			return
		end
	elseif gram.state == 'name' then
		local place = gram.name.place
		local tic = gram.name.tic
		local nex = 0
		if key == 'up' then
			place[tic] = place[tic] + 1
			playSound('rotate')
		elseif key == 'down' then
			place[tic] = place[tic] - 1
			playSound('rotate')
		elseif key == 'left' or key == 'backspace' then
			nex = -1
		elseif key == 'return' then
			nex = 1
		elseif key == 'right' then
			nex = 1
		end

		local bit = 0
		if nex ~= 0 then
			bit = string.byte(key) - 96
			if bit > 0 and bit < 27 and #key == 1 then
				place[tic] = bit
				nex = 1
			end
		end
		if nex == 1 then
			if tic == 3 then
				scorepush()
				playing = false
				stopSound()
				setstate('menu', true)
				return
			end
			tic = math.min(tic + 1, 3)
			playSound('move')
		elseif nex == -1 then
			tic = math.max(tic - 1, 1)
			playSound('move')
		end
		gram.name.place = place
		gram.name.tic = tic
	end
end

function control.resize(w, h)
	-- Generates exact coordinates for touch buttons upon a screen resize.
	if save.touch == 1 then
		touch.coord = {
			{
				key = 'up',
				quad = 1,
				x = screen.scale * save.touchscale / 1.65,
				y = screen.h - screen.scale * save.touchscale * 3.2,
			},
			{
				key = 'down',
				quad = 2,
				x = screen.scale * save.touchscale / 1.65,
				y = screen.h - screen.scale * save.touchscale * 1.2,
			},
			{
				key = 'left',
				quad = 3,
				x = screen.scale * save.touchscale * 0.1,
				y = screen.h - screen.scale * save.touchscale * 2.2,
			},
			{
				key = 'right',
				quad = 4,
				x = screen.scale * save.touchscale * 1.1,
				y = screen.h - screen.scale * save.touchscale * 2.2,
			},
			{
				key = ' ',
				quad = 5,
				x = screen.w - screen.scale * save.touchscale * 1.5,
				y = screen.h - screen.scale * save.touchscale * 2,
			},
		}
	elseif save.touch == 2 then
		touch.coord = {
			{
				key = 'up',
				quad = 1,
				x = screen.w - screen.scale * save.touchscale * 2,
				y = 0,
			},
			{
				key = 'down',
				quad = 2,
				x = 0,
				y = screen.scale * save.touchscale,
			},
			{
				key = 'down',
				quad = 2,
				x = screen.w - screen.scale * save.touchscale,
				y = screen.scale * save.touchscale,
			},
			{
				key = 'left',
				quad = 3,
				x = 0,
				y = 0,
			},
			{
				key = 'right',
				quad = 4,
				x = screen.w - screen.scale * save.touchscale,
				y = 0,
			},
			{
				key = ' ',
				quad = 5,
				x = screen.scale * save.touchscale,
				y = 0,
			},
		}
	elseif save.touch == 3 or save.touch == 5 then
		touch.coord = {}
	elseif save.touch == 4 then
		touch.coord = {
			{
				key = 'down',
				quad = 2,
				x = 0,
				y = screen.scale * save.touchscale,
			},
		}
	end
end

-- Asks if 'button' is being held
function control.getTouch(x, y, button)
	if love.mouse.isDown(1) and touch.coord[button] then
		if
			x > touch.coord[button].x and
			y > touch.coord[button].y and
			x < touch.coord[button].x + save.touchscale * screen.scale and
			y < touch.coord[button].y + save.touchscale * screen.scale then
			return true
		end
	end
	return false
end

-- Returns if keyboard button or matching joystick button is down
function isDown(key)
	local key2 = false
	for i, k in pairs(touch.coord) do
		if control.getTouch(love.mouse.getX(), love.mouse.getY(), i) and key == touch.coord[i].key then
			return true
		end
	end
	if love.keyboard.isDown(key) or key2 then
		return true
	elseif controller.count > 0 then
		for i, k in pairs(save.controller) do
			if save.controller[i].key == key then
				if type(save.controller[i].button) == 'string' then
					if controller.held[save.controller[i].button] then
						return true
					end
				elseif type(save.controller[i].button) == 'number' then
					if controller.object[save.stick]:isDown(save.controller[i].button) then
						return true
					end
				end
			end
		end
	end
	return false
end

-- Returns 'true' if you don't ask for a button and either L or R are held.
function isMouseDown(button)
	if button then
		if love.mouse.isDown(button) then
			return true
		else
			return false
		end
	else
		if love.mouse.isDown(1, 2) then
			return true
		else
			return false
		end
	end
end

function love.keypressed(key)
	if key == 'backspace' then
		love.event.quit()
	elseif key == 'f11' then
		fullscreen()
	elseif keypress[state] then
		keypress[state](key)
	end
end

function love.mousepressed(x, y, button)
	if mousepress[state] then
		mousepress[state](x, y, button)
	end
end

function love.mousereleased(x, y, button)
	if mouserelease[state] then
		mouserelease[state](x, y, button)
	end
end

-- Only used for controller configuration, calls keypress[state]
function love.joystickpressed(joystick, button)
	if type(joystick) == 'number' then
		id = joystick
	else
		id,instance = joystick:getID()
	end
	if id == save.stick then
		if buttonpress[state] then
			buttonpress[state](button)
		end
		if keypress[state] then
			for i, k in ipairs(save.controller) do
				if save.controller[i].button == button then
					keypress[state](save.controller[i].key)
				end
			end
		end
	end
end