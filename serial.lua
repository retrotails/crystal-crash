serial = {}
function serial.pack(t)
	local str = 't = {\n'
	local function convert(var)
		if type(var) == 'boolean' then
			if var then
				return 'true', 'boolean'
			else
				return 'false', 'boolean'
			end
		elseif type(var) =='table' then
			return var, 'table'
		elseif type(var) == 'number' then
			return var, 'number'
		elseif type(var) == 'function' then
			return 'loadstring(\'' .. string.dump(var) .. '\')', 'function'
		elseif type(var) == 'string' then
			return '\'' .. var .. '\'', 'string'
		else
			return 'ERROR: Type = ' .. type(var)
		end
	end
	local function iterate(tab, indent)
		for i, k in pairs(tab) do
			local conv, typ = convert(tab[i])
			if typ == 'table' then
				if type(i) == 'number' then
					str = str .. string.rep('		 ', indent) .. '[' .. i .. ']' .. ' = {\n'
					iterate(tab[i], indent + 1)
					str = str .. string.rep('		 ', indent) .. '},\n'
				else
					str = str .. string.rep('		 ', indent) .. '[\'' .. i .. '\']' .. ' = {\n'
					iterate(tab[i], indent + 1)
					str = str .. string.rep('		 ', indent) .. '},\n'
				end
			else
				str = str .. string.rep('		 ', indent) .. '[\'' .. i .. '\']' .. ' = ' .. conv .. ',\n'
			end
		end
	end
	iterate(t, 1)
	str = string.sub(str, 0, -2) .. '\n}'
	return str
end
function serial.unpack(s)
	loadstring(s)()
	return t
end
