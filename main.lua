getOS = love.system.getOS()
--features = love.graphics.getSupported()

--require 'crash'

math.random = love.math.random

function love.load()
	love.graphics.setDefaultFilter('linear', 'nearest')
	-- Set to true if running but paused
	playing = false
	require 'serial'
	-- Saves / restores settings
	if love.filesystem.getInfo('save.bin') ~= nil then
		save = serial.unpack(love.filesystem.read('save.bin'))
	else
		resetsave()
	end

	controller = {
		refresh = function()
			if love.joystick.getJoystickCount() > 0 then
				controller.count = love.joystick.getJoystickCount()
				controller.object = love.joystick.getJoysticks()
				controller.lastheld = {}
				controller.held = {}
				controller.deadzone = 0.8
				if not controller.object[save.stick] then
					save.stick = 1
				end
			else
				controller.count = 0
			end
		end
	}
	controller.refresh()
	das = {down = .5, left = .5, right = .5}

	mouse = {
		pos = {},
		last = {},
		--first = {},
		crys = {},
		down = 0,
		touchtime = 0.1,
	}

	touch = {size = 32, style = save.touch}

	-- Loads all images in /tex/ recursively
	tex = {}
	local function ass(sys, dir)
		local tab = split(dir, '/')
		table.remove(tab, 1)
		if #tab > 0 then
			local j = tex
			for i, k in pairs(tab) do
				if not tex[tab[i]] then tex[tab[i]] = {} end
				j = tex[tab[i]]
			end
			tab = j
		else
			tab = tex
		end

		for i,k in pairs(sys) do
			if love.filesystem.getInfo(dir .. k).type == 'file' then
				local i = string.sub(k, 1, -5)
				tab[i] = love.graphics.newImage(dir .. k)
			elseif love.filesystem.getInfo(dir .. k).type == 'directory' then
				local fs2 = love.filesystem.getDirectoryItems(dir .. k)
				ass(fs2, dir .. k .. '/')
			end
		end
	end
	local fs = love.filesystem.getDirectoryItems('/tex')
	ass(fs, 'tex/')

	local icon = love.image.newImageData('tex/icon.png')
	love.window.setIcon(icon)

	--Adds contents of /bg to table 'tex', except of course the live backgrounds
	tex.bg = {}
	local fs = love.filesystem.getDirectoryItems('/bg')
	for i,k in pairs(fs) do
		local ext = string.sub(k, -4)
		if ext ~= '.lua' and ext ~= '.shd' then
			tex.bg[k] = love.graphics.newImage('bg/' .. k)
		end
	end

	-- Generates some of the quads
	quad = {
		gem = {},
		flash = {},
		twinkle = {},
		particle = {},
	}
	for i = 0, 8 do
		quad.gem[i+1] = love.graphics.newQuad(i * 16, 0, 16, 16, 144, 32)
	end
	for i = 0, 8 do
		quad.flash[i + 1] = love.graphics.newQuad(i * 16, 16, 16, 16, 144, 32)
	end
	quad.touch = {}
	for i = 0, 4 do
		quad.touch[i + 1] = love.graphics.newQuad(i * 32, 0, 32, 32, 160, 32)
	end
	local text = 'abcdefghijklmnopqrstuvwxyz0123456789 -)(.,'
	quad.text = {}
	for i = 1,#text do
		--quad.text[string.sub(text, i, i)] = love.graphics.newQuad(((i-1)%8)*8, math.ceil(i/8 - 1)*8, 8, 8, 64, 64)
		quad.text[string.sub(text, i, i)] = love.graphics.newQuad(
			((i-1)%8)*8					 + 0.01,
			math.ceil(i/8 - 1)*8 + 0.01,
			7.98, 7.98, 64, 64
		)
	end


	-- Generates burst particle quads
	for i = 0, 8 do
		quad.particle[i+1] = {}
		for x = 0,1 do
			for y = 0,1 do
				quad.particle[i + 1][x*2+y] = love.graphics.newQuad(i * 16 + x, y*8, 8, 8, 144, 32)
			end
		end
	end
	for i = 0, 3 do
		quad.twinkle[i + 1] = love.graphics.newQuad(i * 16, 0, 8, 8, 32, 8)
	end
	canvas = {
		love.graphics.newCanvas(320, 240),
		love.graphics.newCanvas(1280, 960),
	}

	canvas[1]:setFilter('nearest', 'nearest')
	canvas[2]:setFilter('linear', 'linear')

	-- Working memory for each 'virtual machine'
	ram = {}
	-- Game RAM
	gram = {}
	tmpram = false

	-- Current state
	state = 'intro'

	-- The various functions a state can use
	init = {}
	draw = {}
	update = {}
	keypress = {}
	buttonpress = {}
	mousepress = {}
	mouserelease = {}
	resize = {}

	require 'intro'
	require 'menu'
	require 'game'
	require 'control'
	love.resize()

	-- Load all music
	music = {}
	local fs = love.filesystem.getDirectoryItems('music')
	for i,k in pairs(fs) do
		music[string.sub(k, 1, -5)] = love.audio.newSource('music/' .. k, 'stream')
		music[string.sub(k, 1, -5)]:setLooping(true)
		music[string.sub(k, 1, -5)]:setVolume(0.2)
	end

	-- Load all SFX
	sfx = {}
	local fs = love.filesystem.getDirectoryItems('sfx')
	for i,k in pairs(fs) do
		sfx[string.sub(k, 1, -5)] = love.audio.newSource('sfx/' .. k, 'static')
		sfx[string.sub(k, 1, -5)]:setVolume(0.3)
	end

	-- Loads background code, if it's a live BG
	if save.livebg then
		bgcode = assert(loadstring(love.filesystem.read('bg/' .. save.bg)))()
		bgcode.init()
	end


	-- Initializes game
	init[state]()
end
function love.update(dt)
	-- Check for newly connected controllers, update accordingly
	if controller.count ~= love.joystick.getJoystickCount() then
		controller.refresh()
		setstate('menu', true)
		return
	end

	if update[state] then
			update[state](dt)
	end
end

function love.draw()
	if draw[state] then
		draw[state]()
	end
end

function setstate(newstate, clear)
	if clear then ram = nil end
	collectgarbage('collect')
	state = newstate
	init[state]()
end

function love.resize(w, h)
	if not w then w = love.graphics.getWidth() end
	if not h then h = love.graphics.getHeight() end
	screen = {w = w, h = h}
	screen.scale = math.min(screen.w/320, screen.h/240)
	screen.offset= {
		x = 0.5 * (screen.w - screen.scale*320),
		y = 0.5 * (screen.h - screen.scale*240),
	}
	--Minimum screen size is 320x240
	if resize[state] then
		resize[state](w, h)
	end
	if save.livebg and bgcode then
		bgcode.resize(w, h)
	end
	control.resize(w, h)
end

function savedata()
	love.filesystem.write('save.bin', serial.pack(save))
end
function scorepush()
	if scorecheck() then
		local tab = {}
		local c = gram.name.place
		for i = 1,3 do
			if c[i] == 0 then c[i] = 26 end
		end
		tab.name = string.char(c[1] + 96) .. string.char(c[2] + 96) .. string.char(c[3] + 96)
		if save.mode == 'normal' then
			tab.score = gram.score
			tab.level = gram.level
		elseif save.mode == 'flash' then
			tab.score = gram.flashtime
			tab.level = gram.flashlevel
		end
		save.scores[save.mode][save.difficulty] = tab
		gram.name = nil
	end
end
function scorecheck()
	if save.mode == 'normal' then
		if gram.score > save.scores['normal'][save.difficulty].score then
			return true
		end
	elseif save.mode == 'flash' then
		if
			gram.flashlevel > save.scores['flash'][save.difficulty].level
		or
			(
			gram.flashlevel >= save.scores['flash'][save.difficulty].level
			and
			gram.flashtime < save.scores['flash'][save.difficulty].score
			)
		then
			return true
		end
	end
	collectgarbage()
end

-- Sets save to defaults - upon game crashes, savedata is reset as a failsafe
function resetsave()
	save = {
		scores = {
			normal =
			{
				{
					name = '',
					level = 0,
					score = 0,
				},
				{
					name = '',
					level = 0,
					score = 0,
				},
				{
					name = '',
					level = 0,
					score = 0,
				},
			},
			flash =
			{
				{
					name = '',
					level = 0,
					score = 0, -- "Score" here is time.
				},
				{
					name = '',
					level = 0,
					score = 0,
				},
				{
					name = '',
					level = 0,
					score = 0,
				},
			},
		},
		music = 'time',
		touch = 5,
		touchscale = 48,
		sfx = true,
		livebg = true,
		gem = 'default',
		stick = 1,
		difficulty = 2,
		mode = 'normal',
		controller = {
			{name = 'up'		, key = 'up',			button = 'none'},
			{name = 'down'	, key = 'down',		button = 'none'},
			{name = 'left'	, key = 'left',		button = 'none'},
			{name = 'right' , key = 'right',	button = 'none'},
			{name = 'rotate', key = ' ',			button = 'none'},
			{name = 'back'	, key = 'escape', button = 'none'},
		},
	}
	if getOS == 'Android' then
		-- save.music = 'disable'
		-- save.sfx = false
		save.touch = 1
	end
	save.bg = 'default.shd'
	savedata()
end

function playMusic(song)
	if song ~= 'disable' and save.music ~= 'disable' and not music[song]:isPlaying() then
		stopSound()
		music[song]:play()
	elseif song == 'disable' then
		stopSound()
	end
end

function stopSound()
	-- TODO find why this doesn't work
	--love.audio.rewind()
	love.audio.stop()
end

function setMusicVolume(vol)
	if save.music ~= 'disable' then
		music[save.music]:setVolume(vol)
	end
end

function playSound(sound)
	if save.sfx then
		sfx[sound]:stop()
		sfx[sound]:play()
	end
end

function split(str, pat)
	local t = {}
	local fpat = '(.-)' .. pat
	local last_end = 1
	local s, e, cap = str:find(fpat, 1)
	while s do
		if s ~= 1 or cap ~= '' then
			table.insert(t,cap)
		end
		last_end = e+1
		s, e, cap = str:find(fpat, last_end)
	end
	if last_end <= #str then
		cap = str:sub(last_end)
		table.insert(t, cap)
	end
	return t
end

function getTime(seconds)
	local minutes = math.floor(seconds/60)
	local seconds = math.floor(seconds%60)
	if minutes < 10 then minutes = '0' .. minutes end
	if seconds < 10 then seconds = '0' .. seconds end
	return minutes .. 'min\n', seconds .. 'sec'
end

function gameover()
	stopSound()
	if save.music ~= 'disable' then
		playSound('gameover')
	end
	if scorecheck() then
		gram.nextState = 'gameoverhigh'
		-- init variables for name wheel
		gram.name = {
			cursor = {1, 1, 1},
			place = {1, 1, 1},
			tic = 1,
		}
	else
		gram.nextState = 'gameover'
	end
	gram.pause = 5
	love.keyboard.setKeyRepeat(true)
end

function drawText(str, x, y, align, scale)
	if not scale then scale = 1 end
	str = tostring(str)
	local len = #str
	if align == 'center' then
		x = x - len*4*scale
	elseif align == 'right' then
		x = x - len*8
	end
	if not x then x = 0 end
	if not y then y = 0 end
	x,y = math.floor(x), math.floor(y)
	for i = 1,len do
		local quad = quad.text[string.sub(str, i, i)]
		if quad then
			love.graphics.draw(tex.font, quad, x + (i-1)*8*scale, y, 0, scale)
		end
	end
end

function fullscreen()
	if getOS ~= 'Android' then
		local modes = love.window.getFullscreenModes()
		table.sort(modes, function(a, b) return a.width > b.width end)
		local width, height, flags = love.window.getMode( )
		if flags.fullscreen then
			local scale = math.min(math.floor(modes[1].width/320), math.floor(modes[1].height/240))
			if scale < 1 then scale = 1 end
			local x = 320 * scale
			local y = 240 * scale
			love.window.setMode(x, y, {fullscreen = false})
			love.resize(x,y)
		else
			love.window.setMode(modes[1].width,modes[1].height, {fullscreen = true})
			love.resize(modes[1].width,modes[1].height)
		end
	end
end