function init.intro()
	ram = {
		timer = 0,
		maxtime = 1.5,
		skip = function()
			if ram.state == 0 then
				ram.state = 1
				ram.timer = 0
				ram.maxtime = 4
			else
				love.audio.stop()
				setstate('menu')
			end
		end,
		}
	playSound('blip')
end
function update.intro(dt)
	ram.timer = ram.timer + dt
	if ram.timer >= ram.maxtime then
		ram.skip()
	end
end
function draw.intro()
	drawText('pixel potential', screen.w*0.5, screen.h*0.5, 'center', screen.scale)
	drawText('presents', screen.w*0.5, screen.h*0.5 + 32, 'center', screen.scale)
end
function keypress.intro(key)
	ram.skip()
end
function buttonpress.intro(button)
	ram.skip()
end
function mousepress.intro(x, y, button)
	--[[ Press the top left corner during the intro to reset the save in case of emergency
	if x < 32 * screen.scale and y < 32 * screen.scale then
		resetsave()
	end
	--]]
	ram.skip()
end